// 判断注册，登入界面打开,注册界面关闭
function openLogin() {
    let username = $('#userName').val().trim();
    let password = $('#password').val().trim();
    let surepassword = $('#surePassword').val().trim();
    // 前端判断
    if (username == "" || password == "" || surepassword == "") {
        alert('请填写完整信息后再注册！');
        return;
    }
    if (password != surepassword) {
        alert('两次密码不一致！');
        return;
    }

    $.ajax("/register", {
        data: {
            username: username,
            password: password
        },
        type: "post",
        success: function (data) {
            if (data.statusCode == 200) {
                alert("注册成功")
                location.href = "login.html"
                window.close();
            } else if (data.statusCode == 401) {
                alert("注册失败")
            }
        }
    })
}