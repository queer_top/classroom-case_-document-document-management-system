// 页面跳转
var myfile = document.getElementById("toMyFile");
var green = document.getElementById("toGreen");
var red = document.getElementById("toRed");
var blue = document.getElementById("toBlue");

function toMyFile() {
    myfile.style.display = "block";
    green.style.display = "none";
    red.style.display = "none";
    blue.style.display = "none";
}

function toGreen() {
    myfile.style.display = "none";
    green.style.display = "block";
    red.style.display = "none";
    blue.style.display = "none";
}

function toRed() {
    myfile.style.display = "none";
    green.style.display = "none";
    red.style.display = "block";
    blue.style.display = "none";
}

function toBlue() {
    myfile.style.display = "none";
    green.style.display = "none";
    red.style.display = "none";
    blue.style.display = "block";
}

// 创建数据
var pig = "0";
//创建类型描述
var TypeDesc = {
    "dir": "../img/blueFile.png",
    "word": "../img/Blue.png",
    "excel": "../img/Green.png",
    "ppt": "../img/Red.png"
}
//创建数据数组:里面是文件对象
var fileList = [{
    id: "1",
    name: "合同管理",
    imgSrc: "dir",
    pig: "0"
},
    {
        id: "2",
        name: "合同管理",
        imgSrc: "word",
        pig: "0"
    },
    {
        id: "3",
        name: "合同管理",
        imgSrc: "excel",
        pig: "2"
    },
    {
        id: "4",
        name: "合同管理",
        imgSrc: "ppt",
        pig: "2"
    }
];

//显示文件的方法
function showFile() {
    let s = "";
    for (f of fileList) {
        if (f.pig == pig) {
            s += `
						<div class="box" ondblClick="DoubleMouse(${f.id})">
									<input type="checkbox" id='${f.id}' class ="checkboxs"/><br />
									<img src="${TypeDesc[f.imgSrc]}" alt="" /><br /><br />
									<span>${f.name}</span>
								</div>
								`;
        }
    }
    let list1 = document.getElementById("toMyFile");
    list1.innerHTML = s;
}

showFile();

//双击进入文件的方法
function DoubleMouse(id) {
    pig = id;
    showFile();
}

//弹出新增窗口的方法
function AddFile() {
    document.getElementById("addFileUI").style.display = "block";
    document.getElementById("addText").value = "";
    document.getElementById("addType").value = "";
}

//关闭新增弹窗的方法
function offAddUI() {
    document.getElementById("addFileUI").style.display = "none";
}

//新增文件的方法
function okAdd() {
    let filename = document.getElementById("addText").value.trim();
    let filetype = document.getElementById("addType").value.trim();
    if (filename == "") {
        alert("请输入完整!");
        return;
    }
    if (filetype == "") {
        alert("请选择类型!");
        return;
    }
    let f = {
        id: new Date().getTime(), //id只要满足唯一标识就行
        name: filename,
        imgSrc: filetype,
        pig: pig
    };
    fileList.push(f);
    document.getElementById("addFileUI").style.display = "none";
    showFile();
}

//返回文件夹上一级的方法
function BlackFile() {
    for (f of fileList) {
        if (f.id == pig) {
            pig = f.pig;
            break;
        }
    }
    showFile();
}

//全选框的方法
function sAll() {
    var all = document.getElementById("selectall");
    var cboxs = document.getElementsByClassName("checkboxs");
    for (let i = 0; i < cboxs.length; i++) {
        cboxs[i].checked = all.checked;
    }
}

//重命名的方法
function ReFileName() {
    let selectBox = document.querySelectorAll(".box input[type='checkbox']:checked");
    if (selectBox.length > 0) {
        let fname = prompt("请输入新的文件名:");
        for (let i = 0; i < fileList.length; i++) {
            for (let j = 0; j < selectBox.length; j++) {
                let datas = selectBox[j].getAttribute(("id"));
                if (selectBox[j].getAttribute("id") == fileList[i].id) {
                    fileList[i].name = fname;
                }
            }
        }
    }
    showFile();
}

//删除文件的方法
function DelFile() {
    let selectBox = document.querySelectorAll(".box input[type='checkbox']:checked");
    let temp = confirm("确定删除选中的文件吗？");
    if (temp) {
        let sTemp = [];
        for (let i = 0; i < fileList.length; i++) {
            for (let j = 0; j < selectBox.length; j++) {
                let datas = selectBox[j].getAttribute(("id"));
                if (selectBox[j].getAttribute("id") == fileList[i].id) {
                    sTemp.push(i)
                }
            }
        }
        for (let m = sTemp.length - 1; m >= 0; m--) {
            fileList.splice(sTemp[m], 1)
        }
        showFile();
    }
}

function toLoginPage() {
    location.href = "login.html";
    window.close();
}