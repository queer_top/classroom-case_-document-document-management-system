package org.nathan.Dao;

import org.nathan.Utils.JDBCUtils;
import org.nathan.pojo.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * 文件名称：@title: UserDao
 * 项目名称：@projectName: Homework0313
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:用户层数据库连接
 * 创建日期：@date: 2024/3/14 0:43
 */
public class UserDao {
    private static ResultSet rs;
    private static Connection conn;
    private static PreparedStatement statement;

    // 判断登入的方法
    public User login(String name, String pwd) {
        // 构建sql语句
        String loginSql = "select * from t_user where u_name = ? and u_pwd = ? ";
        // 数据库操作
        try {
            conn = JDBCUtils.createConn();
            if (conn != null) {
                statement = conn.prepareStatement(loginSql);
                statement.setString(1, name);
                statement.setString(2, pwd);
                rs = statement.executeQuery();
                if (rs.next()) {
                    User user = new User();
                    user.setUserId(rs.getInt("u_id"));
                    user.setUserName(rs.getString("u_name"));
                    user.setUserPwd(rs.getString("u_pwd"));
                    return user;
                }
            } else {
                System.out.println("未获取到连接...");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeConnect(conn, statement, rs);
        }
        return null;
    }

    // 判断账号是否存在
    public boolean repeat(String name) {
        // 构建sql语句
        String repeatSql = "select u_name from t_user where u_name = ? ";
        // 数据库操作
        try {
            conn = JDBCUtils.createConn();
            if (conn != null) {
                statement = conn.prepareStatement(repeatSql);
                statement.setString(1, name);
                rs = statement.executeQuery();
                if (rs.next()) {
                    return true;
                }
            } else {
                System.out.println("未获取到连接...");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeConnect(conn, statement, rs);
        }
        return false;
    }

    // 数据库插入新账号
    public boolean register(User user) {
        int rs = 0;
        // 构建sql语句
        String insertSql = "insert into t_user values(null,?,?)";
        // 数据库操作
        try {
            conn = JDBCUtils.createConn();
            if (conn != null) {
                statement = conn.prepareStatement(insertSql);
                statement.setString(1, user.getUserPwd());
                statement.setString(2, user.getUserName());
                rs = statement.executeUpdate();
                if (rs > 0) {
                    return true;
                }
            } else {
                System.out.println("未获取到连接...");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeConnect(conn, statement, null);
        }
        return false;
    }
}
