package org.nathan.ServerLet;

import java.util.HashMap;
import java.util.Map;

/**
 * 文件名称：@title: ServerLetFactory
 * 项目名称：@projectName: Homework0313
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:工厂设计模式
 * 创建日期：@date: 2024/3/14 2:34
 */
public class ServerLetFactory {
    private static Map<String, ServerLet> serverLetMap = new HashMap<>();

    static {
        // 完成ServerLet的注册
        serverLetMap.put("/login", new LoginServerLet());
        serverLetMap.put("/register", new RegisterSeverLet());
        serverLetMap.put("/default", new DefaultSerLet());
        serverLetMap.put("/VerCode", new VerCodeServerLet());
    }

    public static ServerLet getSeverLet(String url) {
        ServerLet serverLet = serverLetMap.get(url);
        if (serverLet == null) {
            return serverLetMap.get("/default");
        }
        return serverLet;
    }
}
