package org.nathan.ServerLet;

import com.alibaba.fastjson.JSON;
import org.nathan.Dao.UserDao;
import org.nathan.Http.HttpRequest;
import org.nathan.Http.HttpResponse;
import org.nathan.Server.ServerThread;
import org.nathan.dto.Result;
import org.nathan.pojo.User;

/**
 * 文件名称：@title: loginServerLet
 * 项目名称：@projectName: Homework0313
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:登入业务处理
 * 创建日期：@date: 2024/3/14 1:53
 */
public class LoginServerLet implements ServerLet {
    public void service(HttpRequest httpRequest, HttpResponse httpResponse) {
        //获取用户名和密码
        String username = httpRequest.getRequestParams().get("username");
        String password = httpRequest.getRequestParams().get("password");
        String verCode = httpRequest.getRequestParams().get("verCode");
        String vid = httpRequest.getRequestParams().get("id");
        Result result = new Result();
        // 验证码校验
        String saveCode = ServerThread.vcodeMap.get(vid);
        System.out.println("获取的验证码" + verCode + "====" + "保存的验证码" + saveCode);
        System.out.println(verCode + saveCode);
        if (!verCode.equalsIgnoreCase(saveCode)) {
            result.setStatusCode(201);// 约定验证码错误为201
            result.setErrMsg("验证码有误");
            String s = JSON.toJSONString(result);
            httpResponse.write(s);
            return;
        }
        // 数据库操作
        User user = new UserDao().login(username, password);
        // 响应数据
        if (user != null) {
            result.getData().put("user", user);
        } else {
            result.setErrMsg("用户名密码错误");
            result.setStatusCode(301); //约定301表示登入错误
        }
        String s = JSON.toJSONString(result);
        httpResponse.write(s);
    }
}
