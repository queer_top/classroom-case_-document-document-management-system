package org.nathan.ServerLet;

import org.nathan.Http.HttpRequest;
import org.nathan.Http.HttpResponse;
import org.nathan.Server.ServerThread;
import org.nathan.Utils.CaptchaUtil;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * 文件名称：@title: VerCodeServerLet
 * 项目名称：@projectName: Homework0313
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:获取验证码
 * 创建日期：@date: 2024/3/16 19:19
 */
public class VerCodeServerLet implements ServerLet {
    @Override
    public void service(HttpRequest httpRequest, HttpResponse httpResponse) {
        CaptchaUtil captcha = new CaptchaUtil();
        String codeId = httpRequest.getRequestParams().get("id");
        String code = captcha.getCode();
        System.out.println("验证码id：" + codeId);
        System.out.println("验证码：" + code);
        // 存入验证码
        ServerThread.vcodeMap.put(codeId, code);

        BufferedImage image = captcha.getImage();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            // 将图片写入ByteArrayOutputStream字节数组输出流
            ImageIO.write(image, "png", bos);
            // 从字节数组输出流中获取字节数组
            byte[] data = bos.toByteArray();
            httpResponse.setContentType("image/png");
            httpResponse.write(data);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
