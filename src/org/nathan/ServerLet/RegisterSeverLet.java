package org.nathan.ServerLet;

import com.alibaba.fastjson.JSON;
import org.nathan.Dao.UserDao;
import org.nathan.Http.HttpRequest;
import org.nathan.Http.HttpResponse;
import org.nathan.dto.Result;
import org.nathan.pojo.User;

/**
 * 文件名称：@title: RegisterSeverLet
 * 项目名称：@projectName: Homework0313
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:注册业务处理
 * 创建日期：@date: 2024/3/14 1:59
 */
public class RegisterSeverLet implements ServerLet {
    public void service(HttpRequest httpRequest, HttpResponse httpResponse) {
        //获取用户名和密码
        String username = httpRequest.getRequestParams().get("username");
        String password = httpRequest.getRequestParams().get("password");
        // 数据库操作
        boolean flag = new UserDao().repeat(username);
        User user = new User(null, password, username);
        Result result = new Result();
        // 响应数据
        if (flag) {
            result.setErrMsg("账号已存在");
            result.setStatusCode(401);// 约定账号已存在为401
        } else {
            boolean flag2 = new UserDao().register(user);
            if (flag2) {
                result.getData().put("user", user);
                result.setStatusCode(200);
            } else {
                result.setStatusCode(501);// 约定注册失败错误为501
                result.setErrMsg("注册失败");
            }
        }
        String s = JSON.toJSONString(result);
        httpResponse.write(s);
    }
}
