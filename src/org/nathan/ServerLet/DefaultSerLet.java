package org.nathan.ServerLet;

import org.nathan.Http.HttpRequest;
import org.nathan.Http.HttpResponse;
import org.nathan.Utils.MimeTypeUtil;

import java.io.File;
import java.io.FileInputStream;

/**
 * 文件名称：@title: DefaultSerLet
 * 项目名称：@projectName: Homework0313
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:静态资源处理
 * 创建日期：@date: 2024/3/14 2:00
 */
public class DefaultSerLet implements ServerLet {
    public void service(HttpRequest httpRequest, HttpResponse httpResponse) {
        String mimetype;
        try {
            if ("/".equals(httpRequest.getRequestUrl())) {
                httpRequest.setRequestUrl("/login.html");
            }
            // 静态资源处理
            File file = new File("web" + httpRequest.getRequestUrl());
            if (file.exists()) {
                mimetype = MimeTypeUtil.getMimeType(httpRequest.getRequestUrl());
                FileInputStream fi = new FileInputStream("web" + httpRequest.getRequestUrl());
                byte data[] = new byte[fi.available()];
                fi.read(data);
                httpResponse.setContentType(mimetype);
                httpResponse.write(data);
            } else {
                FileInputStream fi = new FileInputStream("web/404.html");
                byte data[] = new byte[fi.available()];
                fi.read(data);
                httpResponse.setStatusCode(404);
                httpResponse.setStatusMsg("Not Found");
                httpResponse.setContentType("text/html;charset=utf-8");
                httpResponse.write(data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
