package org.nathan.ServerLet;

import org.nathan.Http.HttpRequest;
import org.nathan.Http.HttpResponse;

/**
 * 文件名称：@title: org.nathan.ServerLet
 * 项目名称：@projectName: Homework0313
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:抽象出ServerLet中相同方法的接口
 * 创建日期：@date: 2024/3/14 2:29
 */
public interface ServerLet {
    public void service(HttpRequest httpRequest, HttpResponse httpResponse);
}
