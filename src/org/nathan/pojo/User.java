package org.nathan.pojo;

/**
 * 文件名称：@title: User
 * 项目名称：@projectName: Homework0313
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:简单的用户对象
 * 创建日期：@date: 2024/3/14 1:31
 */
public class User {
    private Integer userId; // 用户id
    private String userPwd; // 用户密码
    private String userName; // 用户昵称

    // 构造函数无参
    public User() {
    }

    // 构造函数有参
    public User(Integer userId, String userPwd, String userName) {
        this.userId = userId;
        this.userPwd = userPwd;
        this.userName = userName;
    }

    // get和set方法
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    // 打印测试
    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", userPwd='" + userPwd + '\'' +
                ", userName='" + userName + '\'' +
                '}';
    }
}
