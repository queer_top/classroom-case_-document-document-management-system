package org.nathan.pojo;

/**
 * 文件名称：@title: File
 * 项目名称：@projectName: Homework0313
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:简单的文档对象
 * 创建日期：@date: 2024/3/15 9:01
 */
public class File {

    private Integer fileId; // 文档id
    private Integer userId; // 文档所属用户id
    private Integer fileStatus; //文档状态：-1表示已删除,1表示正常插入,0表示在在垃圾箱
    private Integer fileStyle; // 文档所属类型id：1：文件夹，2：word，3：excel，4：ppt
    private Integer parentClassId; // 父类文档id，1表示最高
    private String fileName; // 文档名称
    private String fileSize; // 文档大小
    private String createTime; // 文档创建时间
    private String updateTime; // 文档更新时间

    // 无参构造函数
    public File() {
    }

    // 有参构造函数
    public File(Integer fileId, Integer userId, Integer fileStatus, Integer fileStyle, Integer parentClassId, String fileName, String fileSize, String createTime, String updateTime) {
        this.fileId = fileId;
        this.userId = userId;
        this.fileStatus = fileStatus;
        this.fileStyle = fileStyle;
        this.parentClassId = parentClassId;
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    // get和set方法
    public Integer getFileId() {
        return fileId;
    }

    public void setFileId(Integer fileId) {
        this.fileId = fileId;
    }

    public Integer getFileStatus() {
        return fileStatus;
    }

    public void setFileStatus(Integer fileStatus) {
        this.fileStatus = fileStatus;
    }

    public Integer getFileStyle() {
        return fileStyle;
    }

    public void setFileStyle(Integer fileStyle) {
        this.fileStyle = fileStyle;
    }

    public Integer getParentClassId() {
        return parentClassId;
    }

    public void setParentClassId(Integer parentClassId) {
        this.parentClassId = parentClassId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    // 打印测试
    @Override
    public String toString() {
        return "File{" +
                "fileId=" + fileId +
                ", userId=" + userId +
                ", fileStatus=" + fileStatus +
                ", fileStyle=" + fileStyle +
                ", parentClassId=" + parentClassId +
                ", fileName='" + fileName + '\'' +
                ", fileSize='" + fileSize + '\'' +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}
