package org.nathan.dto;

import java.util.HashMap;
import java.util.Map;

/**
 * 文件名称：@title: Result
 * 项目名称：@projectName: Homework0313
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:结果数据传输对象
 * 创建日期：@date: 2024/3/14 3:33
 */
public class Result {
    private int statusCode = 200;//状态码 200:正确,201:验证码错误 ,301:登入时账号密码错误,401:注册账号已存在,501:注册失败
    private String errMsg;// 错误消息
    private Map<String, Object> data = new HashMap<>();// 数据

    // get和set方法
    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }
}
