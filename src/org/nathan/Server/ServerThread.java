package org.nathan.Server;

import org.nathan.Http.HttpRequest;
import org.nathan.Http.HttpResponse;
import org.nathan.ServerLet.ServerLet;
import org.nathan.ServerLet.ServerLetFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * 文件名称：@title: ServerThread
 * 项目名称：@projectName: Homework0313
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:服务端接收线程
 * 创建日期：@date: 2024/3/13 22:24
 */
public class ServerThread extends Thread {
    public static Map<String, String> vcodeMap = new HashMap<>();
    private Socket socket;
    private InputStream is;
    private OutputStream os;

    public ServerThread(Socket socket) {
        this.socket = socket;
        try {
            is = socket.getInputStream();
            os = socket.getOutputStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                byte buf[] = new byte[8192];
                int result = is.read(buf);
                if (result < 0) {
                    socket.close();
                    break;
                }
                String s = new String(buf, 0, result);
                //完成http协议解析
                HttpRequest httpRequest = new HttpRequest(s);
                HttpResponse httpResponse = new HttpResponse(os);
                ServerLet serverLet = ServerLetFactory.getSeverLet(httpRequest.getRequestUrl());
                serverLet.service(httpRequest, httpResponse);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}

//                System.out.println("请求方法" + httpRequest.getRequestMethod());
//                System.out.println("请求头" + httpRequest.getRequestHeaders());
//                System.out.println("请求路径" + httpRequest.getRequestUrl());
//                System.out.println("请求体" + httpRequest.getRequestBody());
//                System.out.println("请求行" + httpRequest.getRequestLine());
//                System.out.println("请求参数" + httpRequest.getRequestParams());